<?php

namespace Drupal\efm;

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedInterface;
use Drupal\inline_entity_form\Subform\FormStateNestingInterface;

final class EntityAncestors implements SandboxedInterface {

  /**
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $ancestors;

  /**
   * EntityAncestors constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $ancestors
   */
  public function __construct(array $ancestors) {
    $this->ancestors = $ancestors;
  }

  public function get(int $index) {
    return $this->ancestors[$index] ?? NULL;
  }

  public static function fromFormState(FormStateInterface $formState) {
    $ancestors = [];
    $i=0;
    do {
      $formObject = $formState->getFormObject();
      if ($formObject instanceof EntityFormInterface) {
        $ancestors[$i] = $formObject->getEntity();
      }
      $formState = ($formState instanceof FormStateNestingInterface)
        ? $formState->getParentFormState() : NULL;
      $i += 1;
    } while ($formState);
    return new static($ancestors);
  }

}
