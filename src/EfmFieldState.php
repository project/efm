<?php

namespace Drupal\efm;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\expression\Context\ContextBagInterface;

final class EfmFieldState {

  use StringTranslationTrait;

  /**
   * @var \Drupal\efm\EfmFieldConfig
   */
  private $efmFieldConfig;

  /**
   * @var mixed
   */
  private $newValue = NULL;

  /**
   * @var bool
   */
  private $hidden = FALSE;

  /**
   * @var bool
   */
  private $disabled = FALSE;

  /**
   * @var string[]
   */
  private $warningCodes = [];

  /**
   * @var string[]
   */
  private $errorCodes = [];

  /**
   * @var string
   */
  private $redirectPath = '';

  /**
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  private $cacheability;

  /**
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  private $redirectPathCacheability;

  /**
   * EppFieldState constructor.
   *
   * @param \Drupal\efm\EfmFieldConfig $efmFieldConfig
   */
  public function __construct(EfmFieldConfig $efmFieldConfig) {
    $this->efmFieldConfig = $efmFieldConfig;
    $this->cacheability = new CacheableMetadata();
    $this->redirectPathCacheability = new CacheableMetadata();
  }

  /**
   * @return \Drupal\efm\EfmFieldConfig
   */
  public function getEppFieldConfig(): EfmFieldConfig {
    return $this->efmFieldConfig;
  }


  public static function create(EfmFieldConfig $efmFieldConfig) {
    return new static($efmFieldConfig);
  }

  public function extractVariables(ContextBagInterface $contextBag, string $fieldName, ?MessengerInterface $maybeDebugMessenger) {
    $this->newValue = $contextBag->getContext('alter_value')->getContextValue();
    $this->hidden = (bool) $contextBag->getContext('alter_hidden')->getContextValue();
    $this->disabled = (bool) $contextBag->getContext('alter_disabled')->getContextValue();
    $this->errorCodes = $contextBag->getContext('error_codes')->getContextValue() ?? [];
    if (is_string($this->errorCodes)) {
      $this->errorCodes = [$this->errorCodes];
    }
    if (!is_array($this->errorCodes)) {
      if ($maybeDebugMessenger) {
        $maybeDebugMessenger->addError($this->t('Invalid non-array valud for EFM error_codes for field %field.', ['%field' => $fieldName]));
      }
      $this->errorCodes = [];
    }
    $this->warningCodes = $contextBag->getContext('warning_codes')->getContextValue() ?? [];
    if (is_string($this->warningCodes)) {
      $this->warningCodes = [$this->warningCodes];
    }
    if (!is_array($this->warningCodes)) {
      if ($maybeDebugMessenger) {
        $maybeDebugMessenger->addError($this->t('Invalid non-array valud for EFM warning_codes for field %field.', ['%field' => $fieldName]));
      }
      $this->warningCodes = [];
    }
    $this->redirectPath = (string) $contextBag->getContext('redirect_path')->getContextValue();
    $this->cacheability = (new CacheableMetadata())
      ->addCacheableDependency($contextBag->getContext('alter_value'))
      ->addCacheableDependency($contextBag->getContext('alter_hidden'))
      ->addCacheableDependency($contextBag->getContext('alter_disabled'))
      ->addCacheableDependency($contextBag->getContext('warning_codes'))
      ->addCacheableDependency($contextBag->getContext('error_codes'))
      ->addCacheableDependency($contextBag->getContext('redirect_path'))
      ;
    $this->redirectPathCacheability = (new CacheableMetadata())
      ->addCacheableDependency($contextBag->getContext('redirect_path'));
  }

  /**
   * @return mixed
   */
  public function getNewValue() {
    return $this->newValue;
  }

  /**
   * @return bool
   */
  public function getHidden(): bool {
    return $this->hidden;
  }

  /**
   * @return bool
   */
  public function getDisabled(): bool {
    return $this->disabled;
  }

  /**
   * @return string[]
   */
  public function getWarningCodes(): array {
    return $this->warningCodes;
  }

  /**
   * @return string[]
   */
  public function getErrorCodes(): array {
    return $this->errorCodes;
  }

  /**
   * @return string
   */
  public function getRedirectPath(): string {
    return $this->redirectPath;
  }

  /**
   * @return \Drupal\Core\Cache\CacheableMetadata
   */
  public function getCacheability(): CacheableMetadata {
    return $this->cacheability;
  }

  /**
   * @return \Drupal\Core\Cache\CacheableMetadata
   */
  public function getRedirectPathCacheability(): CacheableMetadata {
    return $this->redirectPathCacheability;
  }

}
