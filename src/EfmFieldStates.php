<?php

namespace Drupal\efm;

use Drupal\Component\Graph\Graph;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Form\FormStateInterface;

final class EfmFieldStates {

  /**
   * @var \Drupal\efm\EfmFieldState[]
   */
  private $efmFieldStates;

  /**
   * @var FieldableEntityInterface|null
   */
  private $originalEntity;

  /**
   * EppFieldStates constructor.
   *
   * @param \Drupal\efm\EfmFieldState[] $efmFieldStates
   */
  public function __construct(array $efmFieldStates) {
    $this->efmFieldStates = $efmFieldStates;
  }

  public static function fromFormState(FormStateInterface $formState): ?EfmFieldStates {
    $efmFieldStates =& $formState->get('efm_field_states');
    if ($efmFieldStates instanceof EfmFieldStates) {
      return $efmFieldStates;
    }
    return NULL;
  }

  public function toFormState(FormStateInterface $formState): void {
    $formState->set('efm_field_states', $this);
  }

  /**
   * @return \Drupal\efm\EfmFieldState[]
   */
  public function getAll(): array {
    return $this->efmFieldStates;
  }

  /**
   * @param \Drupal\Core\Entity\FieldableEntityInterface $originalEntity
   */
  public function setOriginalEntity(FieldableEntityInterface $originalEntity): void {
    $this->originalEntity = $originalEntity;
  }

  /**
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   */
  public function getOriginalEntity(): FieldableEntityInterface {
    if (!$this->originalEntity) {
      throw new \RuntimeException('Attempt to get original entity before setting it.');
    }
    return $this->originalEntity;
  }


  public static function createFromEntity(FieldableEntityInterface $entity): EfmFieldStates {
    // Assemble an unordered array of efmFieldStates.
    foreach ($entity->getFields(FALSE) as $fieldName => $field) {
      if (
        !isset($efmFieldStates[$fieldName])
        && ($fieldDefinition = $field->getFieldDefinition())
        && $fieldDefinition instanceof FieldConfigInterface
      ) {
        $efmFieldConfig = EfmFieldConfig::fromFieldDefinition($fieldDefinition);
        $efmFieldState = EfmFieldState::create($efmFieldConfig);
        $efmFieldStates[$fieldName] = $efmFieldState;
      }
    }

    // Order graph by efmFieldStates dependencies.
    $graph = [];
    foreach ($efmFieldStates as $fieldName => $efmFieldState) {
      foreach ($efmFieldState->getEppFieldConfig()->getPremises() as $premiseName) {
        $graph[$premiseName]['edges'][$fieldName] = 1;
        $graph += [$fieldName => []];
        if ($premiseEppFieldState = $efmFieldStates[$premiseName]) {
          $premiseEppFieldState->getEppFieldConfig()->addDependee($fieldName);
        }
      }
    }

    // Order efmFieldStates by graph.
    $graph = (new Graph($graph))->searchAndSort();
    uasort($graph, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
    $efmFieldStates = array_replace(array_intersect_key($graph, $efmFieldStates), $efmFieldStates);

    return new static($efmFieldStates);
  }

}
