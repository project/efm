<?php

namespace Drupal\efm;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\expression\Context\ContextBag;
use Drupal\expression\Context\ContextBagInterface;
use Drupal\expression\Expression\ExpressionListServiceInterface;
use Symfony\Component\HttpFoundation\Request;

class EfmFieldConfig {

  use StringTranslationTrait;

  /**
   * @var string
   */
  private $fieldName;

  /**
   * @var string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  private $fieldLabel;

  /**
   * @var string[]
   */
  private $expressions;

  /**
   * @var string[]
   */
  private $premises;

  /**
   * @var string[]
   */
  private $messages;

  /**
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  private $cacheability;

  /**
   * @var string[]
   */
  private $dependees = [];

  /**
   * EfmFieldConfig constructor.
   *
   * @param string $fieldName
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $fieldLabel
   * @param string[] $expressions
   * @param string[] $premises
   * @param string[] $messages
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheability
   */
  public function __construct(string $fieldName, $fieldLabel, array $expressions, array $premises, array $messages, CacheableMetadata $cacheability) {
    $this->fieldName = $fieldName;
    $this->fieldLabel = $fieldLabel;
    $this->expressions = $expressions;
    $this->premises = $premises;
    $this->messages = $messages;
    $this->cacheability = $cacheability;
  }

  public static function fromFieldDefinition(FieldConfigInterface $fieldDefinition) {
    return new static(
      $fieldDefinition->getName(),
      $fieldDefinition->getLabel(),
      $fieldDefinition->getThirdPartySetting('efm', 'expressions', []),
      $fieldDefinition->getThirdPartySetting('efm', 'premises', []),
      $fieldDefinition->getThirdPartySetting('efm', 'messages', []),
      (new CacheableMetadata())->addCacheableDependency($fieldDefinition)
    );
  }

  /**
   * @return string
   */
  public function getFieldName(): string {
    return $this->fieldName;
  }

  /**
   * @return string[]
   */
  public function getExpressions(): array {
    return $this->expressions;
  }

  /**
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getFieldLabel() {
    return $this->fieldLabel;
  }

  /**
   * @return string[]
   */
  public function getPremises(): array {
    return $this->premises;
  }

  /**
   * @return string[]
   */
  public function getSanitizedMessage(string $code): ?MarkupInterface {
    return !empty($this->messages[$code]) ?
      Markup::create(Xss::filterAdmin($this->messages[$code])) :
      NULL;
  }

  /**
   * @return \Drupal\Core\Cache\CacheableMetadata
   */
  public function getCacheability(): CacheableMetadata {
    return $this->cacheability;
  }

  /**
   * @return string[]
   */
  public function getDependees(): array {
    return $this->dependees;
  }

  public function addDependee(string $dependee): void {
    $this->dependees[] = $dependee;
    $this->dependees = array_unique($this->dependees);
  }

  public function buildConfigurationForm(array &$form, ExpressionListServiceInterface $expressionListService) {
    $form['expressions'] = [
      '#type' => 'expression_array_of_keyed_expressions',
      '#title' => $this->t('Expressions'),
      '#default_value' => $this->expressions,
      '#expression_list_service' => $expressionListService,
      '#expression_variables' => static::buildVariables(),
    ];
    $form['premises'] = [
      '#type' => 'expression_array_of_lines',
      '#title' => $this->t('Premises'),
      '#description' => $this->t('Add lines containing premise fields.'),
      '#default_value' => $this->premises,
      // @todo Add proper field name validation.
      '#line_pattern' => '[a-z][a-z0-9_]*',
      '#unique' => TRUE,
      '#rows' => 3,
    ];
    $form['messages'] = [
      '#type' => 'expression_array_of_key_value_pairs',
      '#title' => $this->t('Messages'),
      '#description' => $this->t('Add lines like "error_or_warning_code=Some message".'),
      '#default_value' => $this->messages,
    ];
  }

  public static function buildVariables(?string $fieldName = NULL, ?FieldableEntityInterface $entity = NULL, Request $request = NULL, FormStateInterface $formState = NULL): ContextBagInterface {
    // @see \Drupal\efm\EppFieldState::extractVariables
    $variables = (new ContextBag())
      ->addContext('entity', (new Context(
          new ContextDefinition('entity', t('The current entity.')),
          $entity
        ))
      )
      ->addContext('ancestors', (new Context(
          new ContextDefinition('any', t('The entity and its ancestors in a nested form.')),
          $formState ? EntityAncestors::fromFormState($formState) : NULL
        ))
      )
      ->addContext('submitted', (new Context(
          new ContextDefinition('boolean', t('True if the form was submitted.')),
          $formState ? $formState->isSubmitted() : NULL
        ))
      )
      ->addContext('query', (new Context(
          // @todo Create a sandboxed query object with finer grained caching.
          new ContextDefinition('any', t('The request query object.')),
          $request ? $request->query : NULL
        ))
        ->addCacheableDependency((new CacheableMetadata())->setCacheContexts(['url.query']))
      )
      ->addContext('value', (new Context(
          new ContextDefinition('any', t('The field value.'), FALSE),
          $entity ? $entity->get($fieldName)->getValue() : NULL
        ))
      )
      ->addUpdatableContext('alter_value', (new Context(
          new ContextDefinition('any', t('The field value to update.'), FALSE),
          NULL
        ))
      )
      ->addUpdatableContext('alter_hidden', (new Context(
          new ContextDefinition('boolean', t('Set to true to make the field hidden.'), FALSE),
          NULL
        ))
      )
      ->addUpdatableContext('alter_disabled', (new Context(
          new ContextDefinition('boolean', t('Set to true to make the field disabled.'), FALSE),
          NULL
        ))
      )
      ->addUpdatableContext('error_codes', (new Context(
          // @todo Replace 'any' with a more appropriate type.
          new ContextDefinition('any', t('Set to an array of error codes which will be mapped to the corresponding messages. Showing an error on an unsubmitted form will hide the complete form.')),
          []
        ))
      )
      ->addUpdatableContext('warning_codes', (new Context(
          // @todo Replace 'any' with a more appropriate type.
          new ContextDefinition('any', t('Set to an array of warning codes which will be mapped to the corresponding messages.')),
          []
        ))
      )
      ->addUpdatableContext('redirect_path', (new Context(
          new ContextDefinition('string', t('Set to a valid path like "/node/23" to trigger redirection.')),
          ''
        ))
      )
      ->addUpdatableContext('dump', (new Context(
          new ContextDefinition('any', t('Set to true to dump debug information. Set to an integer to change default dump depth of 6.'), FALSE),
          NULL
        ))
      )
    ;
    return $variables;
  }

}
