<?php

namespace Drupal\efm;

use Drupal\ajax_dependency\AjaxDependency;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\EnforcedResponseException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\expression\Expression\ExpressionListServiceFactoryInterface;
use Drupal\inline_entity_form\Subform\FormStateNestingWrapper;
use Drupal\inline_entity_form\Subform\NestedSubformStateInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Epp
 *
 * Notes on form workflow:
 * @link https://www.drupal.org/files/fapi_workflow_7.x_v1.1.png
 *
 * @todo Move dump to explicit setting, not expression variable.
 * @todo Add more actions
 *   - alter_options
 *   - alter_required
 *   - add JS dependencies via state API.
 * @todo Do redirect without exception.
 *
 * - getForm
 *   - buildForm
 *     - retrieveForm
 *     - prepareForm
 *       - (hook_form_alter)
 *     - processForm
 *       - setUserInput
 *       - doBuildForm
 *         - setCompleteForm (for form)
 *         - handleInputElement
 *         - #process
 *         - set #array_parents, #access, and friends
 *         - doBuildForm (recursion)
 *         - #after_build
 *       - (if isProcessingInput)
 *         - validateForm
 *         - rebuildForm (if needed)
 *           - retrieveForm
 *           - prepareForm
 *           - doBuildForm
 */
class Efm implements ContainerInjectionInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Form\FormErrorHandlerInterface
   */
  protected $formErrorHandler;

  /**
   * @var \Drupal\expression\Expression\ExpressionListServiceFactoryInterface
   */
  protected $expressionListServiceFactory;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface|null
   */
  protected $maybeDebugMessenger;

  /**
   * Epp constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   * @param \Psr\Log\LoggerInterface $logger
   * @param \Drupal\Core\Form\FormErrorHandlerInterface $formErrorHandler
   * @param \Drupal\expression\Expression\ExpressionListServiceFactoryInterface $expressionListServiceFactory
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\Core\Messenger\MessengerInterface|null $debugMessenger
   */
  public function __construct(RequestStack $requestStack, LoggerInterface $logger, \Drupal\Core\Form\FormErrorHandlerInterface $formErrorHandler, ExpressionListServiceFactoryInterface $expressionListServiceFactory, MessengerInterface $messenger, ?MessengerInterface $debugMessenger) {
    $this->requestStack = $requestStack;
    $this->logger = $logger;
    $this->formErrorHandler = $formErrorHandler;
    $this->expressionListServiceFactory = $expressionListServiceFactory;
    $this->messenger = $messenger;
    $this->maybeDebugMessenger = $debugMessenger;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('logger.channel.efm'),
      $container->get('form_error_handler'),
      $container->get('expression.list_service_factory'),
      $container->get('messenger'),
      $container->get('current_user')->hasPermission('efm: view debug information')
        ? $container->get('messenger') : NULL
    );
  }

  protected function alterConfigEditForm(&$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\EntityFormInterface $formObject */
    $formObject = $form_state->getFormObject();
    // See https://www.drupal.org/node/2818877.
    /** @var \Drupal\field\FieldConfigInterface|\Drupal\Core\Field\FieldConfigInterface $fieldConfig */
    $fieldConfig = $formObject->getEntity();

    // Field config form already has this, but bas field override ui not.
    $form['third_party_settings']['#tree'] = TRUE;
    $form['third_party_settings']['efm']['#type'] = 'fieldset';
    $form['third_party_settings']['efm']['#title'] = t('Entity Form Magick');

    EfmFieldConfig::fromFieldDefinition($fieldConfig)->buildConfigurationForm($form['third_party_settings']['efm'], $this->expressionListServiceFactory->createInstance('efm', [], NULL));
  }

  public function hookEntityPrepareForm(EntityInterface $entity, $operation, FormStateInterface $formState) {
    if (!$entity instanceof FieldableEntityInterface) {
      return;
    }

    // Core invokes this hook only once per form. Change this to have it invoked
    // also on form submissions and ajax submissions.
    // @see \Drupal\Core\Entity\EntityForm::buildForm
    unset($formState->getStorage()['entity_form_initialized']);

    $efmFieldStates = EfmFieldStates::fromFormState($formState);
    if (!$efmFieldStates) {
      // @todo Figure out why this is recreated on form submit (no prob, but ??)
      $efmFieldStates = EfmFieldStates::createFromEntity($entity);
      $efmFieldStates->toFormState($formState);
    }

    // Do our best to guess parents (we may be in a nested entity form).
    // @see \Drupal\inline_entity_form\Form\EntityInlineForm::entityForm
    $formParents = $formState instanceof NestedSubformStateInterface
      ? $formState->getCurrentForm()['#parents'] ?? []
      : [];

    foreach ($efmFieldStates->getAll() as $fieldName => $efmFieldState) {
      if ($expressionList = $efmFieldState->getEppFieldConfig()->getExpressions()) {
        $variables = EfmFieldConfig::buildVariables($fieldName, $entity, $this->requestStack->getCurrentRequest(), $formState);
        // @todo Find a way to dump only once for submitted forms.
        $expressionListService = $this->expressionListServiceFactory->createInstance(
          'efm',
          [],
          $this->logger,
          $this->maybeDebugMessenger,
          "EPP:{$entity->getEntityTypeId()}({$entity->bundle()}):{$entity->id()}:{$fieldName}",
          'dump'
        );
        $expressionListCacheability = $efmFieldState->getEppFieldConfig()->getCacheability();
        $updatedVariables = $expressionListService->evaluate($expressionList, $variables, $expressionListCacheability);
        $efmFieldState->extractVariables($updatedVariables, $fieldName, $this->maybeDebugMessenger);

        // Display warnings and errors.
        foreach ($efmFieldState->getWarningCodes() as $warningCode) {
          $sanitizedWarningMessage = $efmFieldState->getEppFieldConfig()
            ->getSanitizedMessage($warningCode)
            ?? $this->t('%field: Undefined warning with code @code', ['%field' => $fieldName, '@code' => $warningCode]);
          $this->messenger->addWarning($sanitizedWarningMessage);
        }
        $errorCodes = $efmFieldState->getErrorCodes();
        if ($errorCodes) {
          $fieldParents = array_merge($formParents, [$fieldName]);
          $fieldParentsPathName = implode('][', $fieldParents);
          // Form state can only have one error per form path, so collect them.
          $sanitizedErrorMessages = [];
          foreach ($errorCodes as $errorCode) {
            // @see \Drupal\Core\Form\FormStateInterface::setErrorByName
            $sanitizedErrorMessages[] = $efmFieldState->getEppFieldConfig()
              ->getSanitizedMessage($errorCode)
              ?? $this->t('%field: Undefined error with code @code', ['%field' => $fieldName, '@code' => $errorCode]);
          }
          $formState->setErrorByName($fieldParentsPathName, Markup::create(implode('<br>', $sanitizedErrorMessages)));
        }

        $redirectPath = $efmFieldState->getRedirectPath();
        if ($redirectPath) {
          // Throwing is ugly, but see the comment at the end of
          // @see \Drupal\Core\Form\FormBuilder::buildForm
          try {
            $url = Url::fromUserInput($redirectPath);
          } catch (\InvalidArgumentException $e) {}
          if (isset($url)) {
            $redirectResponse = new LocalRedirectResponse($url->toString(), 303);
            $redirectResponse->addCacheableDependency($efmFieldState->getRedirectPathCacheability());
            throw new EnforcedResponseException($redirectResponse);
          }
          else {
            $this->maybeDebugMessenger->addError($this->t('Invalid redirect path: @path', ['@path' => $redirectPath]));
          }
        }

        // If value is non-null, set it and ensure entity is still valid.
        // This value is used to populate field #default_value, which normally
        // is ignored on form submit, as we have user input. Later, we will
        // remove user input for all fields that have premise changes, so this
        // value will be used in that cases.
        // @see \Drupal\efm\Epp::onSubformValidate
        $value = $efmFieldState->getNewValue();
        if (isset($value)) {
          $formerFieldValue = $entity->get($fieldName)->getValue();
          $entity->set($fieldName, $value);
          $violations = $entity->validate()->getByField($fieldName);
          if ($violations->count()) {
            $entity->set($fieldName, $formerFieldValue);
            if ($this->maybeDebugMessenger) {
              /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
              foreach ($violations as $violation) {
                $tArgs = [
                  '%entity_type' => $entity->getEntityTypeId(),
                  '%property_path' => $violation->getPropertyPath(),
                  '%message' => $violation->getMessage(),
                ];
                $this->maybeDebugMessenger->addError($this->t('Invalid value for %entity_type:%property_path: %message', $tArgs));
              }
            }
          }
        }
      }
    }

    // Save original entity, so we can later check what changed.
    // Use ::createDuplicate, as ID is not relevant.
    $efmFieldStates->setOriginalEntity($entity->createDuplicate());
  }

  public function hookFormAlter(array &$form, FormStateInterface $formState, string $formId) {
    // Delegate form alter hooks.
    if ($formId === 'field_config_edit_form' || $formId === 'base_field_override_edit_form') {
      $this->alterConfigEditForm($form, $formState);
    }
    if (
      ($formObject = $formState->getFormObject())
      && $formObject instanceof EntityFormInterface
      && ($entity = $formObject->getEntity())
      && $entity instanceof FieldableEntityInterface
    ) {
      $this->alterEntitySubform($form, $formState);
    }
  }

  public function hookInlineEntityFormEntityFormAlter(array &$form, FormStateInterface $formState, FormStateInterface $subformState) {
    $this->alterEntitySubform($form, $subformState);
  }

  protected function alterEntitySubform(array &$form, FormStateInterface $formState) {
    $efmFieldStates = EfmFieldStates::fromFormState($formState);
    if (!$efmFieldStates instanceof EfmFieldStates) {
      return;
    }

    // If form errors are triggered on unsubmitted form, show them, and hide the
    // complete form.
    if ($formState->getErrors() && !$formState->isSubmitted()) {
      // @fixme This emits missing #parents notices. Maybe move to validate.
      $this->formErrorHandler->handleFormErrors($form, $formState);
      if ($formState->getCompleteForm()) {
        $completeForm =& $formState->getCompleteForm();
      }
      else {
        $completeForm =& $form;
      }
      // Don't mess with #access! First preserve AccessResult cacheability.
      if ($completeForm['#access'] instanceof CacheableDependencyInterface) {
        CacheableMetadata::createFromObject($completeForm['#access'])
          ->applyTo($completeForm);
      }
      foreach (Element::children($completeForm) as $child) {
        $completeForm[$child]['#access'] = FALSE;
      }
    }

    foreach ($efmFieldStates->getAll() as $fieldName => $efmFieldState) {
      if (!isset($form[$fieldName])) {
        continue;
      }
      $fieldWidget =& $form[$fieldName];
      $efmFieldState->getCacheability()->applyTo($fieldWidget);

      foreach ($efmFieldState->getEppFieldConfig()->getPremises() as $premiseName) {
        if (!isset($form[$premiseName])) {
          continue;
        }
        $premiseWidget =& $form[$premiseName];
        if ($fieldWidget && $premiseWidget) {
          // Declare the dependency. AjaxDependency will select input elements
          // in premise form array.
          AjaxDependency::dependsOn($premiseWidget, $fieldWidget, $formState);
          // Launch some voodoo to trigger our validation logic.
          $premiseWidget['#element_validate'][] = [
            $this,
            'onPremiseElementValidate'
          ];
        }
      }

      if ($efmFieldState->getHidden()) {
        // Don't mess with #access! First preserve AccessResult cacheability.
        if ($form[$fieldName]['#access'] instanceof CacheableDependencyInterface) {
          CacheableMetadata::createFromObject($form[$fieldName]['#access'])
            ->applyTo($form[$fieldName]);
        }
        $form[$fieldName]['#access'] = FALSE;
      }
      if ($efmFieldState->getDisabled()) {
        $form[$fieldName]['#disabled'] = TRUE;
      }
    }
  }

  public function onPremiseElementValidate(&$element, FormStateInterface $formState, &$completeForm) {
    // Triggered on element validate of any premise element, it adds our
    // validate handler to the form. Voodoo yay, but simplest that works.
    // Note: after_build is too early for this.

    // @todo Factor this out into a FormTool.
    $validationHandler = [$this, 'onFormValidate'];
    $validationHandlers = $formState->getValidateHandlers();
    if (!in_array($validationHandler, $validationHandlers)) {
      $validationHandlers[] = $validationHandler;
      $formState->setValidateHandlers($validationHandlers);
    }
  }

  public function onFormValidate($form, FormStateInterface $formState) {
    if (class_exists(FormStateNestingWrapper::class)) {
      $subformStates = FormStateNestingWrapper::create($formState)
        ->getSelfAndDescendants();
      foreach ($subformStates as $subformState) {
        $this->onSubformValidate($form, $subformState);
      }
    }
    else {
      $this->onSubformValidate($form, $formState);
    }
  }

  protected function onSubformValidate(&$form, FormStateInterface $formState): void {
    if (
      ($efmFieldStates = EfmFieldStates::fromFormState($formState))
      && !$formState->isRebuilding()
      && ($originalEntity = $efmFieldStates->getOriginalEntity())
    ) {
      $formObject = $formState->getFormObject();
      assert($formObject instanceof ContentEntityFormInterface);

      // Get the updated entity, to compare it with original entity.
      $updatedEntity = $formObject->validateForm($form, $formState);
      $formObject->setEntity($updatedEntity);

      // Remove all user input for dependees of changed premises, so in rebuild
      // they can be populated.
      $input = $formState->getUserInput();
      foreach ($efmFieldStates->getAll() as $fieldName => $efmFieldState) {
        // If field has dependees, so is a premise...
        if (
          $updatedEntity->hasField($fieldName)
          && ($dependees = $efmFieldState->getEppFieldConfig()->getDependees())
        ) {
          // ...and premise value changed...
          $originalValue = $originalEntity->get($fieldName);
          $updatedValue = $updatedEntity->get($fieldName);
          if (!$updatedValue->equals($originalValue)) {
            foreach ($dependees as $dependee) {
              // ...unset form input, use entity value.
              unset($input[$dependee]);
            }
          }
        }
      }
      $formState->setUserInput($input);
      $formState->setRebuild();
    }
  }

}
